<!doctype html>
<html lang="ru">



















    <meta charset="UTF-8">

    <title>SHOPofGAME.xyz - МАГАЗИН РАНДОМНЫХ ВЕЩЕЙ</title>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=2.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Noto+Sans:700" rel="stylesheet">
    <link rel="stylesheet" href="../../common.css">
    <link rel="stylesheet" href="../../contentCases.css">



















    <style>
        #preloader{position:fixed;z-index:5;top:0;right:0;bottom:0;left:0;background:#151619;opacity:1;transition:2s ease;}
        #preloader img{position:absolute;top:50%;left:50%;margin:-55px;}
    </style>

    <div id="preloader">
        <img src="../../i/2.svg">
    </div>








    <!-- body -->
    <div id="header">



        <div>
            <img src="../../i/br.svg" class="logo header__logo">
            <br>
            SHOPofGAME
        </div>



        <nav class="header__nav">
            <ul>
                <li>
                    <a>
                        <img src="../../i/bg.png">
                        <br>
                        ГЛАВНАЯ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bh.png">
                        <br>
                        F.A.Q.
                    </a>
                <li>
                    <a>
                        <img src="../../i/bx.png">
                        <br>
                        ГАРАНТИИ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bj.png">
                        <br>
                        МЫ В ВК
                    </a>
            </ul>
        </nav>




        <div class="header__auth">
            <a class="header-auth__first-item header-auth__first-item_auth-steam" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">STEAM</span>
                <img id="header-auth__icon-steam" class="header-auth__icon-social" src="../../i/bk.png">
            </a>
            <a class="header-auth__second-item header-auth__second-item_auth-vk" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">VK</span>
                <img class="header-auth__icon-social" src="../../i/bl.png">
            </a>
        </div>




    </div><!-- END header page section -->








    <div id="live-drop">



            <div id="live-drop__label">
                <p class="live-drop__first-row-label">LIVE-ДРОПЫ</p>
                <p class="live-drop__second-row-label">ТОП ИГРОКОВ</p>
            </div>



            <ul id="live-drop__list">

                <li>
                    <div class="live-drop__shadow"></div>
                    <img src="" class="live-drop__img-win">
                    <div class="live-drop__popup">
                        <img src="" class="live-drop-popup__img-case">
                        <p class="live-drop-popup__user-name"></p>
                        <p class="live-drop-popup__name-thing"></p>
                    </div>
                </li>

            </ul>




    </div><!-- END header__live-drop section -->








<ul>

    <li id="title-current-point">
        <a href="/">Главная</a>
        <span>Тайное</span>
    </li>

    <li id="baner-case">
        <ul>
            <li id="baner-case__price_sctn1">489 Р</li>
            <li id="baner-case__img-thing_sctn1"><img src="../../i/d.png"></li>
            <li id="baner-case__btn-auth_sctn1">
                АВТОРИЗУЙТЕСЬ
                <div id="baner-case__btn-auth_hover">АВТОРИЗУЙТЕСЬ</div>
            </li>
        </ul>
    </li>

    <li id="wrap-contains-cases">
        <ul class="inner">
            <li id="title-contains-case">
                СОДЕРЖИМОЕ КЕЙСА:
            </li>
            <li id="contains-case">
                <ul>
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i001.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            ИМПЕРАТРИЦА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i002.png">
                        </div>
                        <div class="contains-case__description third-color">
                            FIVE-SEVEN
                            <br>
                            HYPER BEAST
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i003.png">
                        </div>
                        <div class="contains-case__description third-color">
                            P250
                            <br>
                            ПРОЩАЛЬНЫЙ ОСКАЛ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i004.png">
                        </div>
                        <div class="contains-case__description third-color">
                            GLOCK-18
                            <br>
                            WASTELAND REBEL
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i005.png">
                        </div>
                        <div class="contains-case__description third-color">
                            РЕВОЛЬВЕР R8
                            <br>
                            ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i006.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AWP
                            <br>
                            ONI TAIJI
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i007.png">
                        </div>
                        <div class="contains-case__description third-color">
                            DESERT EAGLE
                            <br>
                            ЗОЛОТОЙ КАРП
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i008.png">
                        </div>
                        <div class="contains-case__description third-color">
                            P2000
                            <br>
                            ДУХ ОГНЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i009.png">
                        </div>
                        <div class="contains-case__description third-color">
                            USP-S
                            <br>
                            УБИЙСТВО ПОДТВЕРЖДЕНО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i010.png">
                        </div>
                        <div class="contains-case__description third-color">
                            USP-S
                            <br>
                            NEO-NOIR
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i011.png">
                        </div>
                        <div class="contains-case__description third-color">
                            CZ75-AUTO
                            <br>
                            ВИКТОРИЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i012.png">
                        </div>
                        <div class="contains-case__description third-color">
                            MAC-10
                            <br>
                            НЕОНОВЫЙ ГОНЩИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i013.png">
                        </div>
                        <div class="contains-case__description third-color">
                            GALIL AR
                            <br>
                            ЩЕЛКУНЧИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i014.png">
                        </div>
                        <div class="contains-case__description third-color">
                            FAMAS
                            <br>
                            ROLL CAGE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i015.png">
                        </div>
                        <div class="contains-case__description third-color">
                            SAWED-OFF
                            <br>
                            КРАКЕН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i016.png">
                        </div>
                        <div class="contains-case__description third-color">
                            SSG 08
                            <br>
                            DRAGONFIRE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i017.png">
                        </div>
                        <div class="contains-case__description third-color">
                            SSG 08
                            <br>
                            КРОВЬ В ВОДЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i018.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AUG
                            <br>
                            АКИХАБАРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i019.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AUG
                            <br>
                            ХАМЕЛЕОН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i020.png">
                        </div>
                        <div class="contains-case__description third-color">
                            P90
                            <br>
                            СМЕРТОНОСНЫЕ КОШЕЧКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i021.png">
                        </div>
                        <div class="contains-case__description third-color">
                            P90
                            <br>
                            АЗИМОВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i022.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A1-S
                            <br>
                            ОГОНЬ ЧАНТИКО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i023.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A1-S
                            <br>
                            САЙРЕКС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i024.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A1-S
                            <br>
                            СКОРОСТНОЙ ЗВЕРЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i025.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A1-S
                            <br>
                            ЗОЛОТАЯ СПИРАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i026.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            NEON REVOLUTION
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i027.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A1-S
                            <br>
                            MECHA INDUSTRIES
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i028.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            КРОВАВЫЙ СПОРТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i029.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            ROYAL PALADIN
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i030.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            ПУСТЫННАЯ АТАКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i031.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            РЕНТГЕН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i032.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            ВОЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i033.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            BUZZ KILL
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i034.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            ЗВЕЗДНЫЙ КРЕЙСЕР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i035.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            ДОЖДЬ ИЗ ПУЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i036.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            АЗИМОВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i037.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            ВУЛКАН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i038.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            ТОПЛИВНЫЙ ИНЖЕКТОР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i039.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            ПУСТЫННЫЙ ПОВСТАНЕЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i040.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            ОГНЕННЫЙ ЗМЕЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i041.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            ЯГУАР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i042.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AK-47
                            <br>
                            АКВАМАРИНОВАЯ МЕСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i043.png">
                        </div>
                        <div class="contains-case__description third-color">
                            ПП-19 БИЗОН
                            <br>
                            СУД АНУБИСА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i044.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AWP
                            <br>
                            УДАР МОЛНИИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i045.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AWP
                            <br>
                            СКОРОСТНОЙ ЗВЕРЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i046.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AWP
                            <br>
                            АЗИМОВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i047.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AWP
                            <br>
                            БОЕЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i048.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AWP
                            <br>
                            МЕДУЗА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i049.png">
                        </div>
                        <div class="contains-case__description third-color">
                            AWP
                            <br>
                            ИСТОРИЯ О ДРАКОНЕ
                        </div>
                    </li><!-- END item contains-case -->
                </ul>
            </li> <!-- END contains-case -->

        </ul>
    </li> <!-- END wrap-contains-cases -->

</ul>








    <ul class="stats">
        <li>
            <p class="counter-value open-cases__value">1000000</p>
            ОТКРЫТО КЕЙСОВ
        </li>
        <li class="users">
            <p class="counter-value users__value">500</p>
            ПОЛЬЗОВАТЕЛЕЙ
        </li>
        <li class="online">
            <p class="counter-value online__value">5000</p>
            ОНЛАЙН
        </li>
    </ul>








    <div class="footer">
        <div class="inner">
            <img src="../../i/bs.svg" class="footer__logo">
            <br>
            <div class="footer__copyright">
                <b class="footer-copyright__title">
                    &#169; 2013-2018 <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ
                </b>
                <p class="footer-copyright__about-site">
                    На данном сайте вы можете открыть различные кейсы<br>
                    CS:GO по самым выгодням ценам. Отправка выигрыша происходит<br>
                    в автоматическом режиме посредством ботов Steam<br>
                </p>
            </div>
            <div class="footer__link">
                Техническая поддрежка:<br>
                support@shopofgame.xyz<br>
                <a href="/terms-of-use">Пользовательское соглашение</a>
            </div>
        </div> <!-- END inner -->
    </div> <!-- END footer -->








    <script src="../../script.js"></script>





















</html>