<!doctype html>
<html lang="ru">



















    <meta charset="UTF-8">

    <title>SHOPofGAME.xyz - МАГАЗИН РАНДОМНЫХ ВЕЩЕЙ</title>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=2.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Noto+Sans:700" rel="stylesheet">
    <link rel="stylesheet" href="../../common.css">
    <link rel="stylesheet" href="../../contentCases.css">



















    <style>
        #preloader{position:fixed;z-index:5;top:0;right:0;bottom:0;left:0;background:#151619;opacity:1;transition:2s ease;}
        #preloader img{position:absolute;top:50%;left:50%;margin:-55px;}
    </style>

    <div id="preloader">
        <img src="../../i/2.svg">
    </div>








    <!-- body -->
    <div id="header">



        <div>
            <img src="../../i/br.svg" class="logo header__logo">
            <br>
            SHOPofGAME
        </div>



        <nav class="header__nav">
            <ul>
                <li>
                    <a>
                        <img src="../../i/bg.png">
                        <br>
                        ГЛАВНАЯ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bh.png">
                        <br>
                        F.A.Q.
                    </a>
                <li>
                    <a>
                        <img src="../../i/bx.png">
                        <br>
                        ГАРАНТИИ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bj.png">
                        <br>
                        МЫ В ВК
                    </a>
            </ul>
        </nav>




        <div class="header__auth">
            <a class="header-auth__first-item header-auth__first-item_auth-steam" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">STEAM</span>
                <img id="header-auth__icon-steam" class="header-auth__icon-social" src="../../i/bk.png">
            </a>
            <a class="header-auth__second-item header-auth__second-item_auth-vk" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">VK</span>
                <img class="header-auth__icon-social" src="../../i/bl.png">
            </a>
        </div>




    </div><!-- END header page section -->








    <div id="live-drop">



            <div id="live-drop__label">
                <p class="live-drop__first-row-label">LIVE-ДРОПЫ</p>
                <p class="live-drop__second-row-label">ТОП ИГРОКОВ</p>
            </div>



            <ul id="live-drop__list">

                <li>
                    <div class="live-drop__shadow"></div>
                    <img src="" class="live-drop__img-win">
                    <div class="live-drop__popup">
                        <img src="" class="live-drop-popup__img-case">
                        <p class="live-drop-popup__user-name"></p>
                        <p class="live-drop-popup__name-thing"></p>
                    </div>
                </li>

            </ul>




    </div><!-- END header__live-drop section -->








<ul>

    <li id="title-current-point">
        <a href="/">Главная</a>
        <span>Засекреченное</span>
    </li>

    <li id="baner-case">
        <ul>
            <li id="baner-case__price_sctn1">249 Р</li>
            <li id="baner-case__img-thing_sctn1"><img src="../../i/c.png"></li>
            <li id="baner-case__btn-auth_sctn1">
                АВТОРИЗУЙТЕСЬ
                <div id="baner-case__btn-auth_hover">АВТОРИЗУЙТЕСЬ</div>
            </li>
        </ul>
    </li>

    <li id="wrap-contains-cases">
        <ul class="inner">
            <li id="title-contains-case">
                СОДЕРЖИМОЕ КЕЙСА:
            </li>
            <li id="contains-case">
                <ul>
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i001.png">
                        </div>
                        <div class="contains-case__description second-color">
                            SAWED-OFF
                            <br>
                            WASTELAND PRINCESS
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i002.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P250
                            <br>
                            ЦВЕТА ПРИБОЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i003.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P90
                            <br>
                            ИЗУМРУДНЫЙ ДРАКОН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i004.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P90
                            <br>
                            ХЛАДНОКРОВНЫЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i005.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P90
                            <br>
                            РЕЗНОЙ ПРИКЛАД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i006.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P90
                            <br>
                            ТРЕУГОЛЬНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i007.png">
                        </div>
                        <div class="contains-case__description second-color">
                            РЕВОЛЬВЕР R8
                            <br>
                            ЯНТАРНЫЙ ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i008.png">
                        </div>
                        <div class="contains-case__description second-color">
                            SCAR-20
                            <br>
                            BLOODSPORT
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i009.png">
                        </div>
                        <div class="contains-case__description second-color">
                            SCAR-20
                            <br>
                            КАРДИО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i010.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P250
                            <br>
                            ПОКОЙНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i011.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P250
                            <br>
                            РОСПИСЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i012.png">
                        </div>
                        <div class="contains-case__description second-color">
                            NOVA
                            <br>
                            РАСЦВЕТАЮЩАЯ ВЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i013.png">
                        </div>
                        <div class="contains-case__description second-color">
                            NOVA
                            <br>
                            СКОРОСТНОЙ ЗВЕРЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i014.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P2000
                            <br>
                            КОРТИСЕЙРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i015.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P2000
                            <br>
                            IMPERIAL DRAGON
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i016.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P2000
                            <br>
                            ОКЕАНСКАЯ ПЕНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i017.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P250
                            <br>
                            АЗИМОВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i018.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P250
                            <br>
                            КАРТЕЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i019.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P250
                            <br>
                            ФРАНКЛИН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i020.png">
                        </div>
                        <div class="contains-case__description second-color">
                            SCAR-20
                            <br>
                            САЙРЕКС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i021.png">
                        </div>
                        <div class="contains-case__description second-color">
                            SCAR-20
                            <br>
                            ПЯТНА ОТ КРАСКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i022.png">
                        </div>
                        <div class="contains-case__description second-color">
                            DUAL BERETTAS
                            <br>
                            УДАР КОБРЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i023.png">
                        </div>
                        <div class="contains-case__description second-color">
                            GALIL AR
                            <br>
                            SUGAR RUSH
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i024.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A4
                            <br>
                            АДСКОЕ ПЛАМЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i025.png">
                        </div>
                        <div class="contains-case__description second-color">
                            ПП-19 БИЗОН
                            <br>
                            АЗАРТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i026.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            ХРУСТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i027.png">
                        </div>
                        <div class="contains-case__description second-color">
                            РЕВОЛЬВЕР R8
                            <br>
                            ЛАМА-ПУШКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i028.png">
                        </div>
                        <div class="contains-case__description second-color">
                            FAMAS
                            <br>
                            MECHA INDUSTRIES
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i029.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P90
                            <br>
                            SHALLOW GRAVE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i030.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            DECIMATOR
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i031.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            FEVER DREAM
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i032.png">
                        </div>
                        <div class="contains-case__description second-color">
                            SG 553
                            <br>
                            САЙРЕКС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i033.png">
                        </div>
                        <div class="contains-case__description second-color">
                            SSG 08
                            <br>
                            БОЛЬШАЯ ПУШКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i034.png">
                        </div>
                        <div class="contains-case__description second-color">
                            UMP-45
                            <br>
                            ПЕРВОБЫТНЫЙ САБЛЕЗУБ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i035.png">
                        </div>
                        <div class="contains-case__description second-color">
                            USP-S
                            <br>
                            КАЙМАН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i036.png">
                        </div>
                        <div class="contains-case__description second-color">
                            USP-S
                            <br>
                            ОРИОН

                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i037.png">
                        </div>
                        <div class="contains-case__description second-color">
                            USP-S
                            <br>
                            СЫВОРОТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i038.png">
                        </div>
                        <div class="contains-case__description second-color">
                            XM1014
                            <br>
                            СПОКОЙСТВИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i039.png">
                        </div>
                        <div class="contains-case__description second-color">
                            CZ75-AUTO
                            <br>
                            XIANGLIU
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i040.png">
                        </div>
                        <div class="contains-case__description second-color">
                            NOVA
                            <br>
                            АНТИКВАРИАТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i041.png">
                        </div>
                        <div class="contains-case__description second-color">
                            MP7
                            <br>
                            ЗАКЛЯТЫЙ ВРАГ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i042.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            БАХ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i043.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            КОРТИСЕЙРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i044.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            ЭЛЕКТРИЧЕСКИЙ УЛЕЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i045.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            КРАСНАЯ ЛИНИЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i046.png">
                        </div>
                        <div class="contains-case__description second-color">
                            CZ75-AUTO
                            <br>
                            ЖЕЛТЫЙ ЖАКЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i047.png">
                        </div>
                        <div class="contains-case__description second-color">
                            DESERT EAGLE
                            <br>
                            РЖАВЫЙ КОБАЛЬТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i048.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            ЭЛИТНОЕ СНАРЯЖЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i049.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            ГРАФИТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i050.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AUG
                            <br>
                            SYD MEAD
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i051.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AUG
                            <br>
                            FLEET FLOCK
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i052.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i053.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            СНЕЖНАЯ БУРЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i054.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            ГИДРОПОНИКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i055.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            ПУТЕШЕСТВЕННИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i056.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            БУЙСТВО КРАСОК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i057.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            КРАСНЫЙ ГЛЯНЕЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i058.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            КРАСНАЯ ЛИНИЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i059.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AUG
                            <br>
                            БЕНГАЛЬСКИЙ ТИГР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i060.png">
                        </div>
                        <div class="contains-case__description second-color">
                            CZ75-AUTO
                            <br>
                            НОВАЯ ФУКСИЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i061.png">
                        </div>
                        <div class="contains-case__description second-color">
                            DESERT EAGLE
                            <br>
                            ЗАГОВОР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i062.png">
                        </div>
                        <div class="contains-case__description second-color">
                            GLOCK-18
                            <br>
                            ДУХ ВОДЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i063.png">
                        </div>
                        <div class="contains-case__description second-color">
                            GLOCK-18
                            <br>
                            СУМЕРЕЧНАЯ ГАЛАКТИКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i064.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            СТРАЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i065.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            ХОТ-РОД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i066.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            ШЕДЕВР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i067.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            РЫЦАРЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i068.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A4
                            <br>
                            БЕЗЛЮДНЫЙ КОСМОС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i069.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A4
                            <br>
                            龍王 (КОРОЛЬ ДРАКОНОВ)
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i070.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            АТОМНЫЙ СПЛАВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i071.png">
                        </div>
                        <div class="contains-case__description second-color">
                            GALIL AR
                            <br>
                            ЭКО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i072.png">
                        </div>
                        <div class="contains-case__description second-color">
                            FAMAS
                            <br>
                            ДЖИНН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i073.png">
                        </div>
                        <div class="contains-case__description second-color">
                            FAMAS
                            <br>
                            ОСТАТОЧНОЕ ИЗОБРАЖЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i074.png">
                        </div>
                        <div class="contains-case__description second-color">
                            DESERT EAGLE
                            <br>
                            ГИПНОТИЧЕСКИЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i075.png">
                        </div>
                        <div class="contains-case__description second-color">
                            DESERT EAGLE
                            <br>
                            ДРАКОН-ПРЕДВОДИТЕЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i076.png">
                        </div>
                        <div class="contains-case__description second-color">
                            FIVE-SEVEN
                            <br>
                            ПТИЧЬИ ИГРЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i077.png">
                        </div>
                        <div class="contains-case__description second-color">
                            FIVE-SEVEN
                            <br>
                            ОБЕЗЬЯНЬЕ ДЕЛО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i078.png">
                        </div>
                        <div class="contains-case__description second-color">
                            G3SG1
                            <br>
                            ПОТОК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i079.png">
                        </div>
                        <div class="contains-case__description second-color">
                            G3SG1
                            <br>
                            ПАЛАЧ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i080.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AK-47
                            <br>
                            КАРТЕЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                </ul>
            </li> <!-- END contains-case -->

        </ul>
    </li> <!-- END wrap-contains-cases -->

</ul>








    <ul class="stats">
        <li>
            <p class="counter-value open-cases__value">1000000</p>
            ОТКРЫТО КЕЙСОВ
        </li>
        <li class="users">
            <p class="counter-value users__value">500</p>
            ПОЛЬЗОВАТЕЛЕЙ
        </li>
        <li class="online">
            <p class="counter-value online__value">5000</p>
            ОНЛАЙН
        </li>
    </ul>








    <div class="footer">
        <div class="inner">
            <img src="../../i/bs.svg" class="footer__logo">
            <br>
            <div class="footer__copyright">
                <b class="footer-copyright__title">
                    &#169; 2013-2018 <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ
                </b>
                <p class="footer-copyright__about-site">
                    На данном сайте вы можете открыть различные кейсы<br>
                    CS:GO по самым выгодням ценам. Отправка выигрыша происходит<br>
                    в автоматическом режиме посредством ботов Steam<br>
                </p>
            </div>
            <div class="footer__link">
                Техническая поддрежка:<br>
                support@shopofgame.xyz<br>
                <a href="/terms-of-use">Пользовательское соглашение</a>
            </div>
        </div> <!-- END inner -->
    </div> <!-- END footer -->








    <script src="../../script.js"></script>





















</html>