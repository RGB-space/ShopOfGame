<!doctype html>
<html lang="ru">



















    <meta charset="UTF-8">

    <title>SHOPofGAME.xyz - МАГАЗИН РАНДОМНЫХ ВЕЩЕЙ</title>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=2.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Noto+Sans:700" rel="stylesheet">
    <link rel="stylesheet" href="../../common.css">
    <link rel="stylesheet" href="../../contentCases.css">



















    <style>
        #preloader{position:fixed;z-index:5;top:0;right:0;bottom:0;left:0;background:#151619;opacity:1;transition:2s ease;}
        #preloader img{position:absolute;top:50%;left:50%;margin:-55px;}
    </style>

    <div id="preloader">
        <img src="../../i/2.svg">
    </div>








    <!-- body -->
    <div id="header">



        <div>
            <img src="../../i/br.svg" class="logo header__logo">
            <br>
            SHOPofGAME
        </div>



        <nav class="header__nav">
            <ul>
                <li>
                    <a>
                        <img src="../../i/bg.png">
                        <br>
                        ГЛАВНАЯ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bh.png">
                        <br>
                        F.A.Q.
                    </a>
                <li>
                    <a>
                        <img src="../../i/bx.png">
                        <br>
                        ГАРАНТИИ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bj.png">
                        <br>
                        МЫ В ВК
                    </a>
            </ul>
        </nav>




        <div class="header__auth">
            <a class="header-auth__first-item header-auth__first-item_auth-steam" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">STEAM</span>
                <img id="header-auth__icon-steam" class="header-auth__icon-social" src="../../i/bk.png">
            </a>
            <a class="header-auth__second-item header-auth__second-item_auth-vk" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">VK</span>
                <img class="header-auth__icon-social" src="../../i/bl.png">
            </a>
        </div>




    </div><!-- END header page section -->








    <div id="live-drop">



            <div id="live-drop__label">
                <p class="live-drop__first-row-label">LIVE-ДРОПЫ</p>
                <p class="live-drop__second-row-label">ТОП ИГРОКОВ</p>
            </div>



            <ul id="live-drop__list">

                <li>
                    <div class="live-drop__shadow"></div>
                    <img src="" class="live-drop__img-win">
                    <div class="live-drop__popup">
                        <img src="" class="live-drop-popup__img-case">
                        <p class="live-drop-popup__user-name"></p>
                        <p class="live-drop-popup__name-thing"></p>
                    </div>
                </li>

            </ul>




    </div><!-- END header__live-drop section -->








<ul>

    <li id="title-current-point">
        <a href="/">Главная</a>
        <span>Winter Offensive Weapon Case</span>
    </li>

    <li id="baner-case">
        <ul>
            <li id="baner-case__price">79 Р</li>
            <li id="baner-case__img-thing_plain-case"><img src="../../i/bb.png"></li>
            <li id="baner-case__btn-auth_plain-case">
                АВТОРИЗУЙТЕСЬ
                <div id="baner-case__btn-auth_hover">АВТОРИЗУЙТЕСЬ</div>
            </li>
        </ul>
    </li>

    <li id="wrap-contains-cases">
        <ul class="inner">
            <li id="title-contains-case">
                СОДЕРЖИМОЕ КЕЙСА:
            </li>
            <li id="contains-case">
                <ul>
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="a.png">
                        </div>
                        <div class="contains-case__description fifth-color">
                            ПП-19 БИЗОН
                            <br>
                            СИНИЕ ПОЛУТОНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="b.png">
                        </div>
                        <div class="contains-case__description fifth-color">
                            M249
                            <br>
                            МАГМА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="c.png">
                        </div>
                        <div class="contains-case__description fifth-color">
                            FIVE-SEVEN
                            <br>
                            КАМИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="d.png">
                        </div>
                        <div class="contains-case__description fifth-color">
                            GALIL AR
                            <br>
                            ПЕСЧАНАЯ БУРЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="e.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            ВОСХОДЯЩИЙ ЧЕРЕП
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="f.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ПРИЧАЛ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="g.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ЖЕЛЕЗНАЯ РОЗА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="h.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            ПУЛЬС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i.png">
                        </div>
                        <div class="contains-case__description second-color">
                            P250
                            <br>
                            РОСПИСЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="j.png">
                        </div>
                        <div class="contains-case__description second-color">
                            M4A1-S
                            <br>
                            СТРАЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="k.png">
                        </div>
                        <div class="contains-case__description second-color">
                            AWP
                            <br>
                            КРАСНАЯ ЛИНИЯ

                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="l.png">
                        </div>
                        <div class="contains-case__description third-color">
                            SAWED-OFF
                            <br>
                            КРАКЕН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="m.png">
                        </div>
                        <div class="contains-case__description third-color">
                            M4A4
                            <br>
                            АЗИМОВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="n.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ПАТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="o.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            УЛЬТРАФИОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="p.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="q.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ДАМАССКАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                </ul>
            </li> <!-- END contains-case -->
        </ul>
    </li> <!-- END wrap-contains-cases -->

</ul>








    <ul class="stats">
        <li>
            <p class="counter-value open-cases__value">1000000</p>
            ОТКРЫТО КЕЙСОВ
        </li>
        <li class="users">
            <p class="counter-value users__value">500</p>
            ПОЛЬЗОВАТЕЛЕЙ
        </li>
        <li class="online">
            <p class="counter-value online__value">5000</p>
            ОНЛАЙН
        </li>
    </ul>








    <div class="footer">
        <div class="inner">
            <img src="../../i/bs.svg" class="footer__logo">
            <br>
            <div class="footer__copyright">
                <b class="footer-copyright__title">
                    &#169; 2013-2018 <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ
                </b>
                <p class="footer-copyright__about-site">
                    На данном сайте вы можете открыть различные кейсы<br>
                    CS:GO по самым выгодням ценам. Отправка выигрыша происходит<br>
                    в автоматическом режиме посредством ботов Steam<br>
                </p>
            </div>
            <div class="footer__link">
                Техническая поддрежка:<br>
                support@shopofgame.xyz<br>
                <a href="/terms-of-use">Пользовательское соглашение</a>
            </div>
        </div> <!-- END inner -->
    </div> <!-- END footer -->








    <script src="../../script.js"></script>





















</html>