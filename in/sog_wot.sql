-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 31 2018 г., 09:38
-- Версия сервера: 5.7.19-log
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sog_wot`
--

-- --------------------------------------------------------

--
-- Структура таблицы `counter_game`
--

CREATE TABLE `counter_game` (
  `counter` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `counter_game`
--

INSERT INTO `counter_game` (`counter`) VALUES
(1);

-- --------------------------------------------------------

--
-- Структура таблицы `free_thing`
--

CREATE TABLE `free_thing` (
  `id` tinyint(2) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `price` int(5) DEFAULT NULL,
  `patch_to_img` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `free_thing`
--

INSERT INTO `free_thing` (`id`, `name`, `price`, `patch_to_img`) VALUES
(1, 'РАСШИРИТЬ КАЗАРМУ', 250, '/wot/i/b2.png'),
(2, 'СЛОТ В АНГАРЕ', 250, '/wot/i/b3.png'),
(3, 'КРУПНОПЯТН.. ДВУХЦВ..', 250, '/wot/i/b4.png'),
(4, 'ТРЕХЦВЕТНЫЙ КАМУФЛЯЖ', 250, '/wot/i/b5.png'),
(5, 'ПЯТНИСТЫЙ КАМУФЛЯЖ', 250, '/wot/i/b6.png'),
(6, 'МНОГОЦВЕТНЫЙ СВЕТЛЫЙ..', 250, '/wot/i/b7.png'),
(7, '105-ОКТАНОВЫЙ БЕНЗИН', 50, '/wot/i/b8.png'),
(8, 'АВТОМАТИЧЕСКИЙ ОГНЕ..', 50, '/wot/i/b9.png'),
(9, 'БОЛЬШАЯ АПТЕЧКА', 50, '/wot/i/b0.png'),
(10, 'БОЛЬШОЙ РЕМКОПЛЕКТ', 50, '/wot/i/c0.png'),
(11, 'ЯЩИК КОЛЫ', 50, '/wot/i/c1.png'),
(12, 'ДОППАЕК', 50, '/wot/i/c2.png'),
(13, 'ШОКОЛАД', 50, '/wot/i/c3.png'),
(14, 'БУХТЫ', 50, '/wot/i/c4.png'),
(15, 'КРЕПКИЙ КОФЕ', 50, '/wot/i/c5.png'),
(16, 'ОНИГИРИ', 50, '/wot/i/c6.png'),
(17, 'ПУДИНГ С ЧАЕМ', 50, '/wot/i/c7.png'),
(18, 'УЛУЧШЕННЫЙ РАЦИОН', 50, '/wot/i/c8.png'),
(19, 'КОФЕ С ВЫПЕЧКОЙ', 50, '/wot/i/c9.png');

-- --------------------------------------------------------

--
-- Структура таблицы `gold`
--

CREATE TABLE `gold` (
  `id` tinyint(2) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `price` int(5) DEFAULT NULL,
  `patch_to_img` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gold`
--

INSERT INTO `gold` (`id`, `name`, `price`, `patch_to_img`) VALUES
(1, 'ЗОЛОТО И ПРЕМИУМ: 360 ДНЕЙ', 49000, '/wot/i/a1.png'),
(2, 'ЗОЛОТО', 25000, '/wot/i/aa.png'),
(3, 'ЗОЛОТО', 7000, '/wot/i/bb.png'),
(4, 'ЗОЛОТО', 3500, '/wot/i/cc.png'),
(5, 'ЗОЛОТО', 2000, '/wot/i/dd.png'),
(6, 'ЗОЛОТО', 1300, '/wot/i/ee.png'),
(7, 'ЗОЛОТО', 650, '/wot/i/ff.png'),
(8, 'ЗОЛОТО', 300, '/wot/i/gg.png'),
(9, 'ЗОЛОТО', 150, '/wot/i/hh.png');

-- --------------------------------------------------------

--
-- Структура таблицы `human`
--

CREATE TABLE `human` (
  `id_human` tinyint(4) NOT NULL,
  `id_wot` tinyint(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `balance` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `premium_account`
--

CREATE TABLE `premium_account` (
  `id` tinyint(2) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `price` int(5) DEFAULT NULL,
  `patch_to_img` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `premium_account`
--

INSERT INTO `premium_account` (`id`, `name`, `price`, `patch_to_img`) VALUES
(1, 'ПРЕМИУМ: 360 ДНЕЙ', 24000, 'i/a2.png'),
(2, 'ПРЕМИУМ: 180 ДНЕЙ', 13500, 'i/a3.png'),
(3, 'ПРЕМИУМ: 30 ДНЕЙ', 2500, 'i/a4.png'),
(4, 'ПРЕМИУМ: 7 ДНЕЙ', 1500, 'i/a5.png'),
(5, 'ПРЕМИУМ: 3 ДНЯ', 650, 'i/a6.png'),
(6, 'ПРЕМИУМ: 1 ДЕНЬ', 250, 'i/a7.png');

-- --------------------------------------------------------

--
-- Структура таблицы `real_time`
--

CREATE TABLE `real_time` (
  `online_users` tinyint(4) NOT NULL,
  `all_game` int(6) NOT NULL,
  `all_open_box` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE `session` (
  `id_session` varchar(60) NOT NULL,
  `put_date` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`id_session`, `put_date`) VALUES
('sautl9rvipq9m8hhram8hifidnoov3bn', '2018-01-30 18:52:34'),
('4cmgvq9at2dcutuv9do3nb0q8jvobqkc', '2018-01-30 18:51:58'),
('60hproapof46f4k70gg0a3114gmnjkj5', '2018-01-30 19:30:58'),
('m9k96184si2q3s0jesbl0cdsb4u88deo', '2018-01-30 19:31:25'),
('ovmk8894aqid28fvn2b0qd4vjquial7o', '2018-01-30 20:10:52');

-- --------------------------------------------------------

--
-- Структура таблицы `tank`
--

CREATE TABLE `tank` (
  `id` tinyint(2) NOT NULL,
  `name` varchar(27) DEFAULT NULL,
  `price` int(5) DEFAULT NULL,
  `patch_to_img` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tank`
--

INSERT INTO `tank` (`id`, `name`, `price`, `patch_to_img`) VALUES
(1, 'LOWE', 12500, '/wot/i/a.png'),
(2, 'T34', 12014, '/wot/i/b.png'),
(3, 'FCM 50T', 11914, '/wot/i/c.png'),
(4, 'ИС-6', 11814, '/wot/i/d.png'),
(5, 'STRV S1', 11200, '/wot/i/e.png'),
(6, 'MAUERBRECHER', 11150, '/wot/i/f.png'),
(7, 'T34-3', 11009, '/wot/i/g.png'),
(8, '112', 10509, '/wot/i/h.png'),
(9, 'WZ-120-1G FT', 10209, '/wot/i/i.png'),
(10, 'Т54 ПЕРВЫЙ ОБРАЗЕЦ', 8759, '/wot/i/j.png'),
(11, 'VK 45.03', 8000, '/wot/i/k.png'),
(12, 'M4A1 REVALORISE', 7500, '/wot/i/l.png'),
(13, 'AMX CHASSEEUR DE CHARS', 7455, '/wot/i/m.png'),
(14, 'STA-2', 7405, '/wot/i/n.png'),
(15, 'PANTHER MIT 8,8 CM L/71', 7305, '/wot/i/o.png'),
(16, 'FV4202', 7305, '/wot/i/p.png'),
(17, 'T26E4 SUPERPERSHING', 7209, '/wot/i/q.png'),
(18, 'RHM. SKORPION G', 6700, '/wot/i/r.png'),
(19, 'AT 15A', 6505, '/wot/i/s.png'),
(20, 'KRUPP-STEYR WAFFENTRAGER', 6250, '/wot/i/t.png'),
(21, 'PANTER/M10', 6050, '/wot/i/u.png'),
(22, 'STRV M/4257 ALT A.2', 3700, '/wot/i/v.png'),
(23, 'HT NO.VI', 3750, '/wot/i/w.png'),
(24, 'SKODA T40', 3705, '/wot/i/x.png'),
(25, 'VI PUDEL', 3655, '/wot/i/y.png'),
(26, 'TYPE 64', 3505, '/wot/i/z.png'),
(27, 'СУ-100У', 3250, '/wot/i/0.png'),
(28, 'DICKER MAX', 3205, '/wot/i/1.png'),
(29, 'MATILDA BP', 1705, '/wot/i/2.png'),
(30, 'CHI-NU KAI', 1525, '/wot/i/3.png'),
(31, 'EXCELSIOR', 1515, '/wot/i/4.png'),
(32, 'PZ.KPFW. T 25', 1510, '/wot/i/5.png'),
(33, 'ЧЕРЧИЛЬ III', 1505, '/wot/i/6.png'),
(34, 'МАТИЛЬДА IV', 1500, '/wot/i/7.png'),
(35, 'PZ.S 35', 1000, '/wot/i/8.png'),
(36, 'ВАЛЕНТАЙН II', 1000, '/wot/i/9.png'),
(37, 'PZ.KPFW. T 15', 900, '/wot/i/a0.png'),
(38, 'M22 LOCUST', 900, '/wot/i/a8.png'),
(39, 'T-127', 850, '/wot/i/a9.png'),
(40, 'T2 LIGHT TANK', 750, '/wot/i/b1.png');

-- --------------------------------------------------------

--
-- Структура таблицы `win`
--

CREATE TABLE `win` (
  `id_win` smallint(5) NOT NULL,
  `username_human` varchar(50) DEFAULT NULL,
  `date_win` timestamp NULL DEFAULT NULL,
  `price` smallint(5) NOT NULL,
  `name_win` varchar(50) NOT NULL,
  `patch_to_img` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `win`
--

INSERT INTO `win` (`id_win`, `username_human`, `date_win`, `price`, `name_win`, `patch_to_img`) VALUES
(1, 'KoLoBok_v_TaP', '2018-01-22 22:00:00', 750, 'ЗОЛОТО', 'i/ee.png'),
(2, 'SegaWax', '2018-01-22 22:02:00', 50, 'ЗОЛОТО', 'i/hh.png'),
(3, 'Estell', '2018-01-22 22:05:00', 900, 'PZ.KPFW. T 15', 'i/a0.png'),
(4, 'Andro82', '2018-01-22 22:05:30', 250, 'ПРЕМИУМ: 1 ДЕНЬ', 'i/a7.png'),
(5, 'Raush_116', '2018-01-26 15:46:28', 50, 'ДОППАЕК', 'i/c2.png'),
(6, 'nowic', '2018-01-26 10:46:28', 550, 'ЗОЛОТО', 'i/ff.png');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `free_thing`
--
ALTER TABLE `free_thing`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gold`
--
ALTER TABLE `gold`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `human`
--
ALTER TABLE `human`
  ADD PRIMARY KEY (`id_human`),
  ADD KEY `id_human` (`id_human`);

--
-- Индексы таблицы `premium_account`
--
ALTER TABLE `premium_account`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tank`
--
ALTER TABLE `tank`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `win`
--
ALTER TABLE `win`
  ADD PRIMARY KEY (`id_win`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
