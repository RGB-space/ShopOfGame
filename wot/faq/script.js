function faqListShowHide(faqIdSectionActive) {
    var faqListSectionDescription = document.getElementsByClassName('faq-list__section-description'),
        faqListTitle              = document.getElementsByClassName('faq-list__title'),
        faqIdSectionActive,
        i

    for ( i = 0; i < faqListSectionDescription.length; i++ ) {
        faqListSectionDescription[i].classList.remove('faq-list__section-description_active')
        faqListTitle[i].style.background = 'rgba(16,17,20,.2)'
    }

    faqListTitle[faqIdSectionActive].style.background = '#101114'
    faqListSectionDescription[faqIdSectionActive].classList.toggle('faq-list__section-description_active')

}