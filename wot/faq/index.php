<!DOCTYPE html>
<html lang="ru">








<head>
    <meta charset="UTF-8">

    <title>ShopOfGame - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

    <link rel="stylesheet" href="../common.css">

    <meta name="keywords" content="#TODO">
    <meta name="description" content="#TODO">
</head>








<body>





    <header>
        <div class="live-lenta">
            <div class="inner">
            <a href="" class="live-lenta__text-link">ВЫИГРЫШИ<br>
                <p id="live-lenta__live">lIVE</p>
            </a>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            </div>
        </div>

        <nav class="primary-navbar">
            <div class="inner">
                <a id="logo-header">#LOGO</a>
                <a href="/wot">ГЛАВНАЯ</a>
                <a class="primary-navbar-active-el">ВОПРОСЫ</a>
                <a href="/wot/guarantees">ГАРАНТИИ</a>
                <a id="sign-in" href="#">
                    <!-- button auth -->
                    ВОЙТИ WARGAMING
                </a>
            </div>
        </nav>

    </header>





    <main>
        <h1 id="faq__title">ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ</h1>
        <ul id="faq__list">
            <li class="faq-list__title" onclick="faqListShowHide(0)">1. КАК ЗАБРАТЬ ВЫИГРАННЫЙ ТАНК/ЗОЛОТО?
            <li class="faq-list__section-description">При выигрыше, сразу после нажатия Вами кнопки "Забрать" наша система автоматически отправит Вам золото на аккаунт на нашем сайте. При выигрыше танка, Вам будет отправлено эквивалентное ему количество золота.
            <li class="faq-list__title" onclick="faqListShowHide(1)">2. МЕНЯ НЕ ЗАБАНЯТ В WOT?
            <li class="faq-list__section-description">Вы получаете выигрыши через подарок WoT - официальный и доступный всем способ отправки подарков. Наша система за собственные средства покупает для Вас выигрыш и отправляет его подарком. Таким образом, мы гарантируем, что игра на SHOPOFGAME Вам принесет только положительные эмоции!
            <li class="faq-list__title" onclick="faqListShowHide(2)">3. КАК ПОПОЛНИТЬ БАЛАНС?
            <li class="faq-list__section-description">Нажмите кнопку «Пополнить», в открывшемся окне введите сумму пополнения и нажмите «Пополнить баланс».Вас перенаправит на страницу платежной системы, на которой вам необходимо выбрать любой удобный для вас способ и совершить оплату.
            <li class="faq-list__title" onclick="faqListShowHide(3)">4. НЕ ПРИШЛИ ДЕНЬГИ НА БАЛАНС SHOPOFGAME?
            <li class="faq-list__section-description">В большинстве случаев такое происходит из-за задержек в системе оплаты. К сожалению, мы на это повлиять не можем. В течение часа произойдет зачисление.
            <li class="faq-list__title" onclick="faqListShowHide(4)">5. ЧТО БУДЕТ, ЕСЛИ Я ВЫИГРАЮ ТАНК, КОТОРЫЙ У МЕНЯ УЖЕ ЕСТЬ?
            <li class="faq-list__section-description" onclick="faqListShowHide(5)">Вы получите компенсацию золотом в эквиваленте стоимости этого танка.
        </ul>
    </main>






    <footer class="footer">

        <div class="inner-text">

            <div class="logo footer__logo display-inbl"></div>

            <br>

            <div class="footer__left display-inbl">
                <b class="copyright-title"><!--&copy; 2014-2017 --> <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ</b>
                <p>На данном сайте вы можете открыть различные кейсы</p>
                <p>WoT по самым выгодням ценам. Отправка выиграша происходит</p>
                <p>в автоматическом режиме посредством ботов Wargaming</p>
            </div>

            <div class="footer__right display-inbl">
                <p>Техническая поддержка:<br>support@shopofgame.xyz</p>
                <a href="">Пользовательское соглашение</a>
            </div>

        </div><!-- END inner-text -->

        <div class="footer__pay-method">
            <img src="i/pm.png">
        </div>

    </footer>






    <script src="script.js"></script>
</body>








</html>