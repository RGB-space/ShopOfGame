<!DOCTYPE html>
<html lang="ru">








<head>
    <meta charset="UTF-8">

    <title>ShopOfGame - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

    <link rel="stylesheet" href="../common.css">

    <meta name="keywords" content="#TODO">
    <meta name="description" content="#TODO">
</head>








<body>





    <header>
        <div class="live-lenta">
            <div class="inner">
            <a href="" class="live-lenta__text-link">ВЫИГРЫШИ<br>
                <p id="live-lenta__live">lIVE</p>
            </a>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            <div class="live-lenta__item">
                <p class="item__username"></p>
                <img src="" alt="">
                <p class="item__what-win"></p>
            </div>
            </div>
        </div>

        <nav class="primary-navbar">
            <div class="inner">
                <a id="logo-header">#LOGO</a>
                <a href="/wot">ГЛАВНАЯ</a>
                <a href="/wot/faq/">ВОПРОСЫ</a>
                <a class="primary-navbar-active-el">ГАРАНТИИ</a>
                <a href="" id="sign-in">
                    <!-- button auth -->
                    ВОЙТИ WARGAMING
                </a>
            </div>
        </nav>

    </header>





    <main>

        <h1 id="faq__title">НАШИ ГАРАНТИИ</h1>

        <ul class="guarantees">
            <li>
                <img src="../i/gp.png">
                <h2 class="guarantees__head">ВЫИГРЫВАЙТЕ БЕСПЛАТНО</h2>
                <p class="guarantees__description">Пополняйте счет и получайте бесплатные подарки</p>
            </li>
            <li>
                <img src="../i/do.png">
                <h2 class="guarantees__head">НИКТО НЕ ПРОИГРЫВАЕТ</h2>
                <p class="guarantees__description">Вы гарантированно получите приз при открытии любого кейса!</p>
            </li>
            <li>
                <img src="../i/re.png">
                <h2 class="guarantees__head">БОЛЕЕ 1 МЛН ПОКУПОК</h2>
                <p class="guarantees__description">Нашими услугами воспользовалось свыше 120 000 пользователей. Последние выигрыши Вы можете наблюдать в верху сайта</p>
            </li>
            <li>
                <img src="../i/cl.png">
                <h2 class="guarantees__head">ДОСТАВКА ЗА СЕКУНДУ</h2>
                <p class="guarantees__description">Сразу после выбора кейса, Вы получаете выиграшь на свой аккаунт, благодаря продвинутым ботам SHOPofGAME</p>
            </li>
        </ul>

    </main>






    <footer class="footer">

        <div class="inner-text">

            <div class="logo footer__logo display-inbl"></div>

            <br>

            <div class="footer__left display-inbl">
                <b class="copyright-title"><!--&copy; 2014-2017 --> <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ</b>
                <p>На данном сайте вы можете открыть различные кейсы</p>
                <p>WoT по самым выгодням ценам. Отправка выиграша происходит</p>
                <p>в автоматическом режиме посредством ботов Wargaming</p>
            </div>

            <div class="footer__right display-inbl">
                <p>Техническая поддержка:<br>support@shopofgame.xyz</p>
                <a href="">Пользовательское соглашение</a>
            </div>

        </div><!-- END inner-text -->

        <div class="footer__pay-method">
            <img src="../i/pm.png">
        </div>

    </footer>






    <script src="script.js"></script>
</body>








</html>