<?
    $sql_query_create_table_gold = 'CREATE TABLE gold(
    id_gold             TINYINT(2) PRIMARY KEY,
    name                VARCHAR(20),
    price               INT(5),
    patch_to_img        VARCHAR(20)
    )';

    $sql_query_create_table_premium_days = 'CREATE TABLE premium_account(
    id_premium_account  TINYINT(2) PRIMARY KEY,
    name                VARCHAR(20),
    price               INT(5),
    patch_to_img        VARCHAR(20)
    )';

    $sql_query_create_table_tank = 'CREATE TABLE tank(
    id_tank             TINYINT(2) PRIMARY KEY,
    name                VARCHAR(20),
    price               INT(5),
    patch_to_img        VARCHAR(20)
    )';

    $sql_query_create_table_free_thing = 'CREATE TABLE free_thing(
    id_free_thing       TINYINT(2) PRIMARY KEY,
    name                VARCHAR(20),
    price               INT(5),
    patch_to_img        VARCHAR(20)
    )';

    $sql_query_create_table_user = 'CREATE TABLE user(
    id_user_sog         TINYINT(3) PRIMARY KEY,
    user_name           VARCHAR(50),
    id_user_wot         VARCHAR(20),
    summ_gaming         TINYINT(2)
    ';#TODO





    $sql_query_inset_data_gold = 'INSERT INTO gold VALUES
                                 ( \'1\', \'ЗОЛОТО И ПРЕМИУМ: 360 ДНЕЙ\', \'49000\', \'/wot/i/ш.png\' ),
                                 ( \'2\', \'ЗОЛОТО\', \'25000\', \'/wot/i/щ.png\' ),
                                 ( \'3\', \'ЗОЛОТО\', \'7000\', \'/wot/i/ъ.png\' ),
                                 ( \'4\', \'ЗОЛОТО\', \'3500\', \'/wot/i/ь.png\' ),
                                 ( \'5\', \'ЗОЛОТО\', \'2000\', \'/wot/i/э.png\' ),
                                 ( \'6\', \'ЗОЛОТО\', \'1300\', \'/wot/i/ю.png\' ),
                                 ( \'7\', \'ЗОЛОТО\', \'650\', \'/wot/i/я.png\' ),
                                 ( \'8\', \'ЗОЛОТО\', \'300\', \'/wot/i/aa.png\' ),
                                 ( \'9\', \'ЗОЛОТО\', \'150\', \'/wot/i/bb.png\' )
                                 ';

    $sql_query_inset_data_premium_account = 'INSERT INTO premium_account VALUES
                                 ( \'1\', \'ПРЕМИУМ: 360 ДНЕЙ\', \'24000\', \'/wot/i/ш.png\' ),
                                 ( \'2\', \'ПРЕМИУМ: 180 ДНЕЙ\', \'13500\', \'/wot/i/щ.png\' ),
                                 ( \'3\', \'ПРЕМИУМ: 30 ДНЕЙ\', \'2500\', \'/wot/i/ъ.png\' ),
                                 ( \'4\', \'ПРЕМИУМ: 7 ДНЕЙ\', \'1500\', \'/wot/i/ь.png\' ),
                                 ( \'5\', \'ПРЕМИУМ: 3 ДНЯ\', \'650\', \'/wot/i/э.png\' ),
                                 ( \'6\', \'ПРЕМИУМ: 1 ДЕНЬ\', \'250\', \'/wot/i/ю.png\' )
                                 ';

    $sql_query_inset_data_tank = 'INSERT INTO tank VALUES
                                 ( \'1\', \'LOWE\', \'12500\', \'/wot/i/a.png\' ),
                                 ( \'2\', \'T34\', \'12014\', \'/wot/i/b.png\' ),
                                 ( \'3\', \'FCM 50T\', \'11914\', \'/wot/i/c.png\' ),
                                 ( \'4\', \'ИС-6\', \'11814\', \'/wot/i/d.png\' ),
                                 ( \'5\', \'STRV S1\', \'11200\', \'/wot/i/e.png\' ),
                                 ( \'6\', \'MAUERBRECHER\', \'11150\', \'/wot/i/f.png\' ),
                                 ( \'7\', \'T34-3\', \'11009\', \'/wot/i/g.png\' ),
                                 ( \'8\', \'112\', \'10509\', \'/wot/i/h.png\' ),
                                 ( \'9\', \'WZ-120-1G FT\', \'10209\', \'/wot/i/i.png\' ),
                                 ( \'10\', \'Т54 ПЕРВЫЙ ОБРАЗЕЦ\', \'8759\', \'/wot/i/j.png\' ),
                                 ( \'11\', \'VK 45.03\', \'8000\', \'/wot/i/k.png\' ),
                                 ( \'12\', \'M4A1 REVALORISE\', \'7500\', \'/wot/i/l.png\' ),
                                 ( \'13\', \'AMX CHASSEEUR DE CHARS\', \'7455\', \'/wot/i/m.png\' ),
                                 ( \'14\', \'STA-2\', \'7405\', \'/wot/i/n.png\' ),
                                 ( \'15\', \'PANTHER MIT 8,8 CM L/71\', \'7305\', \'/wot/i/o.png\' ),
                                 ( \'16\', \'FV4202\', \'7305\', \'/wot/i/p.png\' ),
                                 ( \'17\', \'T26E4 SUPERPERSHING\', \'7209\', \'/wot/i/q.png\' ),
                                 ( \'18\', \'RHM. SKORPION G\', \'6700\', \'/wot/i/r.png\' ),
                                 ( \'19\', \'AT 15A\', \'6505\', \'/wot/i/s.png\' ),
                                 ( \'20\', \'KRUPP-STEYR WAFFENTRAGER\', \'6250\', \'/wot/i/t.png\' ),
                                 ( \'21\', \'PANTER/M10\', \'6050\', \'/wot/i/u.png\' ),
                                 ( \'22\', \'STRV M/4257 ALT A.2\', \'3700\', \'/wot/i/v.png\' ),
                                 ( \'23\', \'HT NO.VI\', \'3750\', \'/wot/i/w.png\' ),
                                 ( \'24\', \'SKODA T40\', \'3705\', \'/wot/i/x.png\' ),
                                 ( \'25\', \'VI PUDEL\', \'3655\', \'/wot/i/y.png\' ),
                                 ( \'26\', \'TYPE 64\', \'3505\', \'/wot/i/z.png\' ),
                                 ( \'27\', \'СУ-100У\', \'3250\', \'/wot/i/0.png\' ),
                                 ( \'28\', \'DICKER MAX\', \'3205\', \'/wot/i/1.png\' ),
                                 ( \'29\', \'MATILDA BP\', \'1705\', \'/wot/i/2.png\' ),
                                 ( \'30\', \'CHI-NU KAI\', \'1525\', \'/wot/i/3.png\' ),
                                 ( \'31\', \'EXCELSIOR\', \'1515\', \'/wot/i/4.png\' ),
                                 ( \'32\', \'PZ.KPFW. T 25\', \'1510\', \'/wot/i/5.png\' ),
                                 ( \'33\', \'ЧЕРЧИЛЬ III\', \'1505\', \'/wot/i/6.png\' ),
                                 ( \'34\', \'МАТИЛЬДА IV\', \'1500\', \'/wot/i/7.png\' ),
                                 ( \'35\', \'PZ.S 35\', \'1000\', \'/wot/i/8.png\' ),
                                 ( \'36\', \'ВАЛЕНТАЙН II\', \'1000\', \'/wot/i/9.png\' ),
                                 ( \'37\', \'PZ.KPFW. T 15\', \'900\', \'/wot/i/а.png\' ),
                                 ( \'38\', \'M22 LOCUST\', \'900\', \'/wot/i/б.png\' ),
                                 ( \'39\', \'T-127\', \'850\', \'/wot/i/в.png\' ),
                                 ( \'40\', \'T2 LIGHT TANK\', \'750\', \'/wot/i/г.png\' )
                                 ';

    $sql_query_inset_data_free_thing = 'INSERT INTO free_thing VALUES
                                       ( \'1\', \'РАСШИРИТЬ КАЗАРМУ\', \'250\', \'/wot/i/д.png\' ),
                                       ( \'2\', \'СЛОТ В АНГАРЕ\', \'250\', \'/wot/i/e.png\' ),
                                       ( \'3\', \'КРУПНОПЯТН.. ДВУХЦВ..\', \'250\', \'/wot/i/ж.png\' ),
                                       ( \'4\', \'ТРЕХЦВЕТНЫЙ КАМУФЛЯЖ\', \'250\', \'/wot/i/з.png\' ),
                                       ( \'5\', \'ПЯТНИСТЫЙ КАМУФЛЯЖ\', \'250\', \'/wot/i/и.png\' ),
                                       ( \'6\', \'МНОГОЦВЕТНЫЙ СВЕТЛЫЙ..\', \'250\', \'/wot/i/к.png\' ),
                                       ( \'7\', \'105-ОКТАНОВЫЙ БЕНЗИН\', \'50\', \'/wot/i/л.png\' ),
                                       ( \'8\', \'АВТОМАТИЧЕСКИЙ ОГНЕ..\', \'50\', \'/wot/i/м.png\' ),
                                       ( \'9\', \'БОЛЬШАЯ АПТЕЧКА\', \'50\', \'/wot/i/н.png\' ),
                                       ( \'10\', \'БОЛЬШОЙ РЕМКОПЛЕКТ\', \'50\', \'/wot/i/о.png\' ),
                                       ( \'11\', \'ЯЩИК КОЛЫ\', \'50\', \'/wot/i/п.png\' ),
                                       ( \'12\', \'ДОППАЕК\', \'50\', \'/wot/i/р.png\' ),
                                       ( \'13\', \'ШОКОЛАД\', \'50\', \'/wot/i/с.png\' ),
                                       ( \'14\', \'БУХТЫ\', \'50\', \'/wot/i/т.png\' ),
                                       ( \'15\', \'КРЕПКИЙ КОФЕ\', \'50\', \'/wot/i/у.png\' ),
                                       ( \'16\', \'ОНИГИРИ\', \'50\', \'/wot/i/ф.png\' ),
                                       ( \'17\', \'ПУДИНГ С ЧАЕМ\', \'50\', \'/wot/i/x.png\' ),
                                       ( \'18\', \'УЛУЧШЕННЫЙ РАЦИОН\', \'50\', \'/wot/i/ц.png\' ),
                                       ( \'19\', \'КОФЕ С ВЫПЕЧКОЙ\', \'50\', \'/wot/i/ч.png\' )
                                       ';





    $sql_query_inset_data_gold = "INSERT INTO gold VALUES
                                 ( '1', 'ЗОЛОТО И ПРЕМИУМ: 360 ДНЕЙ', '49000', '/wot/i/ш.png' ),
                                 ( '2', 'ЗОЛОТО', '25000', '/wot/i/щ.png' ),
                                 ( '3', 'ЗОЛОТО', '7000', '/wot/i/ъ.png' ),
                                 ( '4', 'ЗОЛОТО', '3500', '/wot/i/ь.png' ),
                                 ( '5', 'ЗОЛОТО', '2000', '/wot/i/э.png' ),
                                 ( '6', 'ЗОЛОТО', '1300', '/wot/i/ю.png' ),
                                 ( '7', 'ЗОЛОТО', '650', '/wot/i/я.png' ),
                                 ( '8', 'ЗОЛОТО', '300', '/wot/i/aa.png' ),
                                 ( '9', 'ЗОЛОТО', '150', '/wot/i/bb.png' )
                                 ";

    $sql_query_inset_data_premium_account = "INSERT INTO premium_account VALUES
                                 ( '1', 'ПРЕМИУМ: 360 ДНЕЙ', '24000', '/wot/i/ш.png' ),
                                 ( '2', 'ПРЕМИУМ: 180 ДНЕЙ', '13500', '/wot/i/щ.png' ),
                                 ( '3', 'ПРЕМИУМ: 30 ДНЕЙ', '2500', '/wot/i/ъ.png' ),
                                 ( '4', 'ПРЕМИУМ: 7 ДНЕЙ', '1500', '/wot/i/ь.png' ),
                                 ( '5', 'ПРЕМИУМ: 3 ДНЯ', '650', '/wot/i/э.png' ),
                                 ( '6', 'ПРЕМИУМ: 1 ДЕНЬ', '250', '/wot/i/ю.png' )
                                 ";

    $sql_query_inset_data_tank = "INSERT INTO tank VALUES
                                 ( '1', 'LOWE', '12500', '/wot/i/a.png' ),
                                 ( '2', 'T34', '12014', '/wot/i/b.png' ),
                                 ( '3', 'FCM 50T', '11914', '/wot/i/c.png' ),
                                 ( '4', 'ИС-6', '11814', '/wot/i/d.png' ),
                                 ( '5', 'STRV S1', '11200', '/wot/i/e.png' ),
                                 ( '6', 'MAUERBRECHER', '11150', '/wot/i/f.png' ),
                                 ( '7', 'T34-3', '11009', '/wot/i/g.png' ),
                                 ( '8', '112', '10509', '/wot/i/h.png' ),
                                 ( '9', 'WZ-120-1G FT', '10209', '/wot/i/i.png' ),
                                 ( '10', 'Т54 ПЕРВЫЙ ОБРАЗЕЦ', '8759', '/wot/i/j.png' ),
                                 ( '11', 'VK 45.03', '8000', '/wot/i/k.png' ),
                                 ( '12', 'M4A1 REVALORISE', '7500', '/wot/i/l.png' ),
                                 ( '13', 'AMX CHASSEEUR DE CHARS', '7455', '/wot/i/m.png' ),
                                 ( '14', 'STA-2', '7405', '/wot/i/n.png' ),
                                 ( '15', 'PANTHER MIT 8,8 CM L/71', '7305', '/wot/i/o.png' ),
                                 ( '16', 'FV4202', '7305', '/wot/i/p.png' ),
                                 ( '17', 'T26E4 SUPERPERSHING', '7209', '/wot/i/q.png' ),
                                 ( '18', 'RHM. SKORPION G', '6700', '/wot/i/r.png' ),
                                 ( '19', 'AT 15A', '6505', '/wot/i/s.png' ),
                                 ( '20', 'KRUPP-STEYR WAFFENTRAGER', '6250', '/wot/i/t.png' ),
                                 ( '21', 'PANTER/M10', '6050', '/wot/i/u.png' ),
                                 ( '22', 'STRV M/4257 ALT A.2', '3700', '/wot/i/v.png' ),
                                 ( '23', 'HT NO.VI', '3750', '/wot/i/w.png' ),
                                 ( '24', 'SKODA T40', '3705', '/wot/i/x.png' ),
                                 ( '25', 'VI PUDEL', '3655', '/wot/i/y.png' ),
                                 ( '26', 'TYPE 64', '3505', '/wot/i/z.png' ),
                                 ( '27', 'СУ-100У', '3250', '/wot/i/0.png' ),
                                 ( '28', 'DICKER MAX', '3205', '/wot/i/1.png' ),
                                 ( '29', 'MATILDA BP', '1705', '/wot/i/2.png' ),
                                 ( '30', 'CHI-NU KAI', '1525', '/wot/i/3.png' ),
                                 ( '31', 'EXCELSIOR', '1515', '/wot/i/4.png' ),
                                 ( '32', 'PZ.KPFW. T 25', '1510', '/wot/i/5.png' ),
                                 ( '33', 'ЧЕРЧИЛЬ III', '1505', '/wot/i/6.png' ),
                                 ( '34', 'МАТИЛЬДА IV', '1500', '/wot/i/7.png' ),
                                 ( '35', 'PZ.S 35', '1000', '/wot/i/8.png' ),
                                 ( '36', 'ВАЛЕНТАЙН II', '1000', '/wot/i/9.png' ),
                                 ( '37', 'PZ.KPFW. T 15', '900', '/wot/i/а.png' ),
                                 ( '38', 'M22 LOCUST', '900', '/wot/i/б.png' ),
                                 ( '39', 'T-127', '850', '/wot/i/в.png' ),
                                 ( '40', 'T2 LIGHT TANK', '750', '/wot/i/г.png' )
                                 ";

    $sql_query_inset_data_free_thing = "INSERT INTO free_thing VALUES
                                       ( '1', 'РАСШИРИТЬ КАЗАРМУ', '250', '/wot/i/д.png' ),
                                       ( '2', 'СЛОТ В АНГАРЕ', '250', '/wot/i/e.png' ),
                                       ( '3', 'КРУПНОПЯТН.. ДВУХЦВ..', '250', '/wot/i/ж.png' ),
                                       ( '4', 'ТРЕХЦВЕТНЫЙ КАМУФЛЯЖ', '250', '/wot/i/з.png' ),
                                       ( '5', 'ПЯТНИСТЫЙ КАМУФЛЯЖ', '250', '/wot/i/и.png' ),
                                       ( '6', 'МНОГОЦВЕТНЫЙ СВЕТЛЫЙ..', '250', '/wot/i/к.png' ),
                                       ( '7', '105-ОКТАНОВЫЙ БЕНЗИН', '50', '/wot/i/л.png' ),
                                       ( '8', 'АВТОМАТИЧЕСКИЙ ОГНЕ..', '50', '/wot/i/м.png' ),
                                       ( '9', 'БОЛЬШАЯ АПТЕЧКА', '50', '/wot/i/н.png' ),
                                       ( '10', 'БОЛЬШОЙ РЕМКОПЛЕКТ', '50', '/wot/i/о.png' ),
                                       ( '11', 'ЯЩИК КОЛЫ', '50', '/wot/i/п.png' ),
                                       ( '12', 'ДОППАЕК', '50', '/wot/i/р.png' ),
                                       ( '13', 'ШОКОЛАД', '50', '/wot/i/с.png' ),
                                       ( '14', 'БУХТЫ', '50', '/wot/i/т.png' ),
                                       ( '15', 'КРЕПКИЙ КОФЕ', '50', '/wot/i/у.png' ),
                                       ( '16', 'ОНИГИРИ', '50', '/wot/i/ф.png' ),
                                       ( '17', 'ПУДИНГ С ЧАЕМ', '50', '/wot/i/x.png' ),
                                       ( '18', 'УЛУЧШЕННЫЙ РАЦИОН', '50', '/wot/i/ц.png' ),
                                       ( '19', 'КОФЕ С ВЫПЕЧКОЙ', '50', '/wot/i/ч.png' )
                                       ";

?>